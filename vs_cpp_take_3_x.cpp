#include <iostream>

/**
* @brief �뢥��� ����� ��� ��� �᫠ �� 0 �� N.
* @param [in] N - �� 楫�� �᫮, �� ���ண� �믮������ �����.
* @param [in] isOdd - �ᯮ���� 0 (false) ��� ���� ���祭�� � 1 (true) ��� ������
**/
void printOddEven(const int N, bool isOdd)
{
    for (int i = isOdd; i < N; i += 2)
    {
        std::cout << i << " ";
    }
    std::cout << "\n";
}

int main()
{
    const int N{ 15 };
    std::cout << "�� ��� �᫠ �� 0 �� N:\n";
    for (int i = 0; i < N; ++i)
    {
        if (!(i % 2)) std::cout << i << " ";
    }
    std::cout << "\n";

    //����� ���⪨� ��ਠ��:
    for (int i = 0; i < N; i += 2)
    {
        std::cout << i << " ";
    }
    std::cout << "\n";

    //�㭪�� (odds printing)"
    std::cout << "�㭪樮����� ��� (�����):\n";
    printOddEven(N, true);

    std::cout << "�㭪樮����� ��� (���):\n";
    printOddEven(N, false);
}